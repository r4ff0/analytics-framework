//
//  AnalyticsConfig.swift
//  Analytics
//
//  Created by Rafał Kasprzyk on 09.10.2016.
//  Copyright © 2016 Rafał Kasprzyk. All rights reserved.
//

import Foundation

public class AnalyticsConfig {
    let gai: GAI
    var id: String?
    
    public init() {
        self.gai = GAI.sharedInstance()
    }
    
    public func track(withId: String) {
        self.id = withId
    }
    
    public func dryRun(status: Bool = false) {
        self.gai.dryRun = status
    }
    
    public func trackUncaughtExceptions(status: Bool = true) {
        self.gai.trackUncaughtExceptions = status
    }
    
    public func setLogLevel(level: GAILogLevel) {
        self.gai.logger.logLevel = level
    }
}
