//
//  AnalyticsTrackExceptions.swift
//  Analytics
//
//  Created by Rafał Kasprzyk on 12.10.2016.
//  Copyright © 2016 Rafał Kasprzyk. All rights reserved.
//

import Foundation

extension AnalyticsHelper {
    public func track(exception: String, isFatal: Bool = false, params: [String: Any] = [:]) {
        self.tracker.set(kGAIScreenName, value: self.name.rawValue)
        
        self.prepare(params: params)
        
        let builder = GAIDictionaryBuilder.createException(withDescription: exception, withFatal: NSNumber(value: isFatal))
        
        if let data = builder?.build() {
            self.send(data: data)
        }
    }
}
