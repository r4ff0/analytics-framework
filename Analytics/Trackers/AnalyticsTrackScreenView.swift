//
//  AnalyticsTrackScreenView.swift
//  Analytics
//
//  Created by Rafał Kasprzyk on 12.10.2016.
//  Copyright © 2016 Rafał Kasprzyk. All rights reserved.
//

import Foundation

extension AnalyticsHelper {
    public func track(screen: AnalyticsCategory.Screen, params: [String: Any] = [:]) {
        self.name = screen
        self.tracker.set(kGAIScreenName, value: screen.rawValue)
        
        if let devideId = UIDevice.current.identifierForVendor?.uuidString, self.tracker.get(kGAIUserId) != nil {
            self.tracker.set(kGAIUserId, value: devideId)
        }
        
        self.prepare(params: params)
        
        let builder = GAIDictionaryBuilder.createScreenView()
        
        if let data = builder?.build() {
            self.send(data: data)
        }
    }
}
