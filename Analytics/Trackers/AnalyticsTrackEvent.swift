//
//  AnalyticsTrackEvent.swift
//  Analytics
//
//  Created by Rafał Kasprzyk on 12.10.2016.
//  Copyright © 2016 Rafał Kasprzyk. All rights reserved.
//

import Foundation

extension AnalyticsHelper {
    public func track(action: AnalyticsCategory.Action, forCategory: AnalyticsCategory.Category, label: String, value: NSNumber?, params: [String: Any] = [:]) {
        self.tracker.set(kGAIScreenName, value: self.name.rawValue)
        
        self.prepare(params: params)
        
        let builder = GAIDictionaryBuilder.createEvent(withCategory: forCategory.rawValue, action: action.rawValue, label: label, value: value)
        
        if let data = builder?.build() {
            self.send(data: data)
        }
        
        self.tracker.set(kGAIScreenName, value: nil)
    }
}
