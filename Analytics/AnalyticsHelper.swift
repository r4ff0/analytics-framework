//
//  AnalyticsHelper.swift
//  Analytics
//
//  Created by Rafał Kasprzyk on 07.10.2016.
//  Copyright © 2016 Rafał Kasprzyk. All rights reserved.
//

import Foundation

enum AnalyticsHelperError: Error {
    case invalidTracker
    case invalidTrackerId
}

public class AnalyticsHelper: AnalyticsHelperProtocol {
    // MARK: - Properties
    internal var name: AnalyticsCategory.Screen = .unknown
    
    internal let config: AnalyticsConfig
    internal let tracker: GAITracker
    
    // MARK: - Init with config injection
    public init(config: AnalyticsConfig) throws {
        self.config = config
        
        guard let trackerId = self.config.id else {
            throw AnalyticsHelperError.invalidTrackerId
        }
        
        guard let tracker = self.config.gai.tracker(withName: "ND-\(trackerId)", trackingId: trackerId) else {
            throw AnalyticsHelperError.invalidTracker
        }
        
        self.tracker = tracker
    }
    
    // MARK: - Get tracker instance
    public func getTracker() -> GAITracker {
        return self.tracker
    }
    
    // MARK: - Internal functions
    internal func prepare(params: [String: Any]) {
        if !params.isEmpty {
            for key in params.keys {
                if let value = params[key] as? String {
                    self.tracker.set(key, value: value)
                }
            }
        }
    }
    
    internal func send(data: NSMutableDictionary) {
        var analyticsData = [AnyHashable: Any]()
        
        for key in data.allKeys {
            analyticsData[key as! String] = data[key]
        }
        
        self.tracker.send(analyticsData)
    }
}
