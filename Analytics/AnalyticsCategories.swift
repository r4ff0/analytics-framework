//
//  AnalyticsCategories.swift
//  Analytics
//
//  Created by Rafał Kasprzyk on 07.10.2016.
//  Copyright © 2016 Rafał Kasprzyk. All rights reserved.
//

import Foundation

public enum AnalyticsCategory {
    public enum Screen: String {
        case list = "VideosListView"
        case item = "VideosItemView"
        case add = "AddVideoView"
        case comments = "CommentVideoView"
        case search = "SearchView"
        case settings = "SettingsView"
        case unknown = "Unknown"
    }
    
    public enum Category: String {
        case stream = "Stream"
        case page = "Page"
        case videos = "Videos"
        case video = "Video"
        case search = "Search"
        case settings = "Settings"
    }
    
    public enum Action: String {
        case load = "Load"
        case show = "Show"
        case refresh = "Refresh"
        case search = "Search"
        case add = "Add"
        case comment = "Comment"
    }
}
