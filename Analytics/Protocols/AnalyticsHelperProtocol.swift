//
//  AnalyticsHelperProtocol.swift
//  Analytics
//
//  Created by Rafał Kasprzyk on 12.10.2016.
//  Copyright © 2016 Rafał Kasprzyk. All rights reserved.
//

import Foundation

protocol AnalyticsHelperProtocol {
    func getTracker() -> GAITracker
    
    func prepare(params: [String: Any])
    func send(data: NSMutableDictionary)
}
