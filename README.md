# Analytics # 
This framework was created to simplify the usage of Google Analytics iOS SDK.

[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)

## How to install? ##
First of all you need to add framework to the project. You can do that by selecting Files -> Add files to "Your project name" or you can use [Carthage](https://github.com/Carthage/Carthage).

To configure and use AnalyticsHelper you can for instance write singleton class like this one below. Remember to add **import Analytics** after the *import Foundation* and change "YOUR_TRACKING_ID" to your tracking ID obtained from Google Analytics service.
```
#!swift
import Foundation
import Analytics

struct Analytics {
    static let tracker: AnalyticsHelper? = {
        // Setup analytics
        let config = AnalyticsConfig()
        config.track(withId: "YOUR_TRACKING_ID")
        config.dryRun(status: true)
        config.setLogLevel(level: .verbose)
        config.trackUncaughtExceptions(status: true)

        do {
            return try AnalyticsHelper(config: config)
        } catch {
            print("Analytics helper: Error")
            
            return nil
        } 
    }()
}
```
Apart from tracking ID are three other configuration options available.
```
#!swift
config.dryRun(status: true)
```
This method prevents data to be sent to Google Analytics. This option should be used when you want to test or debug something.
```
#!swift
config.setLogLevel(level: .verbose)
```
Set the level of info messages delivered by SDK and displayed in the console. There are four possible levels of logging: error, warning, info and verbose.
```
#!swift
config.trackUncaughtExceptions(status: true)
```
Track uncaught exceptions allows google analytics library to track and measure exceptions that may occur in your application. You can read more about it [here](https://developers.google.com/analytics/devguides/collection/ios/v3/exceptions)

### Errors ###
*AnalyticsHelper* class throws two exceptions that you should handle. 
```
#!swift
enum AnalyticsHelperError: Error {
    case invalidTracker
    case invalidTrackerId
}
```
One exception is thrown when no tracking id was provided and the other when GAI library will fail to create tracker object.

## How to use? ##
There are three trackers available now namely tracking screen views, events and exceptions.

All available screens, categories and actions that may be tracked can be defined in *AnalyticsCategories.swift* file. You should adjust it to your needs.

All track methods can take params parameter that are then send as additional data to Google Analytics service.

Example:
```
#!swift
let params = ["key": "test_key", "value": "test_value"]
        
Analytics.tracker?.track(screen: .list, params: params)
```


### Screen views tracking ###
You can track screen views by invoking below method with the option defined in *AnalyticsCategories.Screen* enum.
```
#!swift
Analytics.tracker?.track(screen: .list)
```

### Event tracking ###
If you want to track some events for instance page load you can call below method. Some example parameters were shown for a given case of usage. This method uses *AnalyticsCategories.Action* and *AnalyticsCategories.Category* enums data.
```
#!swift
Analytics.tracker?.track(action: .load, forCategory: .page, label: "page 1", value: 1)
```

### Exception tracking ###
If you want to track and log exceptions with Google Analytics below method should be used. You should provide description of an exception and define it status as fatal or non-fatal. Default value of the ***isFatal*** parameter is *false*.
```
#!swift
Analytics.tracker?.track(exception: "Exception description", isFatal: false)
```

# License #

Analytics framework is released under the MIT license:
[www.opensource.org/licenses/MIT](www.opensource.org/licenses/MIT)